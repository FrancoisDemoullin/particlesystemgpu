#pragma once

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h>

class Shader
{
public:
	GLuint Program;

	//Constructor
	Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
	
	//Destructor // nothing to do
	inline ~Shader() {};

	// Uses the current shader
	void use();
};
