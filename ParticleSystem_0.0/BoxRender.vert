#version 330 core

layout (location = 0) in vec4 vertices;

// unusual but necessary
uniform sampler2D position_texture;
uniform sampler2D goal_texture;

uniform vec3 CameraRight_worldspace;
uniform vec3 CameraUp_worldspace;

uniform mat4 view;
uniform mat4 projection;

out vec4 Color;

void main()
{
	float particleSize = 0.1f; // magic number

	vec2 uv_id = vertices.zw;

	// get the position from the texture
	vec3 particleCenter_worldspace = texture(position_texture, uv_id).xyz;
	vec3 goalPos = texture(goal_texture, uv_id).xyz;

	// particles facing the camera
	vec3 vertexPosition_worldspace = particleCenter_worldspace + CameraRight_worldspace * vertices.x * particleSize + CameraUp_worldspace * vertices.y * particleSize;

	// find the final position
	vec4 final_pos = projection * view * vec4(vertexPosition_worldspace, 1.0f);
	gl_Position = final_pos;
	
	//gl_Position = vec4(vertices.xy, 0.0f, 1.0f);

	if(goalPos.z <= -5.0f)
	{
		Color = vec4(0.0f, 0.0f, 1.0f, 1.0f);
	}
	else if(goalPos.z >= 5.0f)
	{
		Color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
	}
	else
	{
		float linearInterpolation = (goalPos.z + 5.0f) / 10.0f;
		Color = vec4(linearInterpolation, 0.0f, 1.0f - linearInterpolation, 1.0f);
	}
}