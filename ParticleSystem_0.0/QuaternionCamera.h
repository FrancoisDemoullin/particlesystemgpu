#pragma once

#include "Quat.h"

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

static const GLfloat SENSITIVITY = 0.1f;

class QuatCamera 
{
public:

	QuatCamera(glm::vec3 pPosition = glm::vec3(10.0f, 0.0f, 0.0f));

	void setViewByMouse(float xOffset, float yOffset);
	glm::mat4 getViewMatrix();
	inline glm::vec3 getUp() { return mUp; }
	inline glm::vec3 getPos() { return mPosition; }
	inline glm::vec3 getView() { return mViewVec; }

private:
	glm::vec3 mUp;
	glm::vec3 mPosition;
	glm::vec3 mViewVec;

	double lCurrentRotationX;

	void rotateCamera(double pAngle, double pX, double pY, double pZ);
	
};