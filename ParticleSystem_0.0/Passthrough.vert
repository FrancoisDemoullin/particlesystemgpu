#version 330 core
layout (location = 0) in vec3 vertices;
layout (location = 1) in vec2 uv;

out vec2 UV;

void main()
{
	gl_Position = vec4(vertices.xyz, 1.0f);
	UV = uv;
}