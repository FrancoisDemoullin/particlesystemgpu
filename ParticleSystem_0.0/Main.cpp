// Std. Includes
#include <string>

// GLEW
#define GLEW_STATIC
#include <Gl/glew.h>
#include <GLFW/glfw3.h>

// my includes
#include "Shader.h"
#include "Camera.h"
#include "Text_Renderer.h"
#include "global.h"
#include "PS.h"
#include "QuaternionCamera.h"

//freeType
#include <ft2build.h>
#include FT_FREETYPE_H  

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtc/random.hpp>

#define _USE_MATH_DEFINES
#include <math.h>

// Window Properties
GLuint global::gScreenWidth = 800;
GLuint global::gScreenHeight = 600;
float global::gDeltaTime = 0.0;
glm::mat4 global::gProjection;
glm::mat4 global::gView;
bool global::gNext = false;
bool global::gDisAssemble = false;
bool global::gDisAssemble2 = false;
float global::gIncrement = 0.0;

static bool pause = false;

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void executeMovements();

// Camera - 2 FPS Camera models are implemented
//Camera* global::gCamera = new Camera(glm::vec3(10.0f, 0.0f, 20.0f));
QuatCamera* global::gCamera = new QuatCamera(glm::vec3(0.0f, 0.0f, 20.0f));

bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

// we need to keep track of time
GLfloat lastFrame = 0.0f;

// CONSTANTS
const int TEXTURE_SIZE = 1024; // total # of particles => TEXTURE_SIZE * TEXTURE_SIZE 
// 1524 -> 2.5mio
// 1024 -> 1 mio

const float LIFE = 80.0f;
const int SPREAD_VALUE = 10.0;
const float EMITS_PER_SECOND = TEXTURE_SIZE/20;

int LastUsedParticle = 0;

//debugging information only
int numberOfFloorCollisions = 0;

int numberOfFrames = 0;
float previousTime = glfwGetTime();

int main(){

	// Init GLFW
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	
	GLFWwindow* window = glfwCreateWindow(global::gScreenWidth, global::gScreenHeight, "PSGPU", nullptr, nullptr); // Windowed
	glfwMakeContextCurrent(window);

	// Set the callback functions
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// Options
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Initialize GLEW to setup the OpenGL Function pointers
	glewExperimental = GL_TRUE;
	glewInit();

	// Defining the viewport dimensions
	glViewport(0, 0, global::gScreenWidth, global::gScreenHeight);

	// enable depth
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS); //GL_LESS used by default anyways

	// set up the text renderer
	//TextRenderer textRenderer(global::gScreenWidth, global::gScreenHeight);

	// Textures
	glEnable(GL_TEXTURE_2D);

	size_t lGoalPositionSize = 512;
	GLfloat* lGoalPosition = (GLfloat*)malloc(lGoalPositionSize * lGoalPositionSize * 3 * sizeof(GLfloat));

	for (int i = 0; i <= (lGoalPositionSize * lGoalPositionSize * 3); i = i + 3)
	{
		if (i < (lGoalPositionSize * lGoalPositionSize * 3) / 6)
		{
			// front face
			lGoalPosition[i] = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10)) - 5.0f;
			lGoalPosition[i + 1] = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10)) - 5.0f;
			lGoalPosition[i + 2] = -5.0f;
		}
		else if (i < 2 * (lGoalPositionSize * lGoalPositionSize * 3) / 6)
		{
			//back face
			lGoalPosition[i] = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10)) - 5.0f;
			lGoalPosition[i + 1] = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10)) - 5.0f;
			lGoalPosition[i + 2] = 5.0f;
		}
		else if (i < 3 * (lGoalPositionSize * lGoalPositionSize * 3) / 6)
		{
			// left face
			lGoalPosition[i] = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10)) - 5.0f;
			lGoalPosition[i + 1] = -5.0f;
			lGoalPosition[i + 2] = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10)) - 5.0f;
		}
		else if (i < 4 * (lGoalPositionSize * lGoalPositionSize * 3) / 6)
		{
			// right face
			lGoalPosition[i] = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10)) - 5.0f;
			lGoalPosition[i + 1] = 5.0f;
			lGoalPosition[i + 2] = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10)) - 5.0f;
		}
		else  //if (i < 5 * (mSize * mSize * 3) / 6)
		{
			lGoalPosition[i] = 0.0f;
			lGoalPosition[i + 1] = 0.0f;
			lGoalPosition[i + 2] = 20.0f;
		}
	}
	
	//PS lPS(512, 100.0, lGoalPosition, lGoalPositionSize,  "Passthrough.vert", "PositionUpdate.frag", "BoxAssembleVelocity.vert", "BoxAssembleVelocity.frag", "BoxRender.vert", "Render.frag");
	PS lPS(512, 100.0, lGoalPosition, lGoalPositionSize, "Passthrough.vert", "PositionUpdate.frag", "BoxAssembleVelocity.vert", "BoxAssembleVelocity.frag", "BoxRender.vert", "Render.frag");

	size_t lNextGoalPositionSize = 256;
	GLfloat* lNextGoalPosition = (GLfloat*)malloc(lGoalPositionSize * lGoalPositionSize * 3 * sizeof(GLfloat));

	for (int i = 0; i <= (lNextGoalPositionSize * lNextGoalPositionSize * 3); i = i + 3)
	{
		if (i % 2 == 0) 
		{
			lNextGoalPosition[i] = -5.0f;
			lNextGoalPosition[i + 1] = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10)) - 5.0f;
			lNextGoalPosition[i + 2] = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10)) - 5.0f;
		}
		else 
		{
			lNextGoalPosition[i] = 5.0f;
			lNextGoalPosition[i + 1] = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10)) - 5.0f;
			lNextGoalPosition[i + 2] = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10)) - 5.0f;

		}
	}
	//PS lNextPS(256, 100.0, lNextGoalPosition, lNextGoalPositionSize, "Passthrough.vert", "PositionUpdate.frag", "BoxAssembleVelocity.vert", "BoxAssembleVelocity.frag", "BoxRender.vert", "Render.frag");
	PS lNextPS(256, 100.0, lNextGoalPosition, lNextGoalPositionSize, "Passthrough.vert", "PositionUpdate.frag", "BoxAssembleVelocity.vert", "BoxAssembleVelocity.frag", "NextRender.vert", "Render.frag");

	// not working for some reason
	//free(lGoalPosition);

	// Draw in wireframe
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	float fps = 0;

	lastFrame = glfwGetTime();

	// Game loop
	while (!glfwWindowShouldClose(window))
	{
		// Set frame time
		GLfloat currentFrame = glfwGetTime();
		global::gDeltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// calculate the frame rate
		
		numberOfFrames++;
		if (currentFrame - previousTime >= 1.0f)
		{
			fps = numberOfFrames;
			numberOfFrames = 0;
			previousTime = currentFrame;
		}

		// Check for events and respond accordingly
		glfwPollEvents();
		executeMovements();

		// Clear the colorbuffer

		// black background
		glClearColor(0.05f, 0.05f, 0.05f, 1.0f);

		// white background
		//glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//glEnable(GL_BLEND);
		//glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

		// update matrices
		//global::gProjection = glm::perspective(global::gCamera->Zoom, (float)global::gScreenWidth / (float)global::gScreenHeight, 0.1f, 100.0f);
		//global::gView = global::gCamera->getViewMatrix();

		// update + render the particles
		lPS.updatePass();
		lPS.renderPass();

		if (global::gNext) 
		{
			lNextPS.updatePass();
			lNextPS.renderPass();
		}

		if (global::gDisAssemble) 
		{
			
			lPS.hotSwapVelocityShader("BoxAssembleVelocity.vert", "BoxFountainVelocity.frag");
			lNextPS.hotSwapVelocityShader("BoxAssembleVelocity.vert", "BoxFountainVelocity.frag");
			global::gDisAssemble = false;
		}

		if (global::gDisAssemble2) 
		{
			global::gIncrement = 0.0;
			lPS.hotSwapVelocityShader("BoxAssembleVelocity.vert", "BoxFountainVelocity2.frag");
			lNextPS.hotSwapVelocityShader("BoxAssembleVelocity.vert", "BoxFountainVelocity2.frag");
			global::gDisAssemble2 = false;
		}

		glfwSwapBuffers(window);
	}
	glfwTerminate();
	return 0;
}

#pragma region "User input"

// Moves/alters the camera positions based on user input
void executeMovements()
{
	/*
	// Camera controls
	if (keys[GLFW_KEY_W])
		global::gCamera->processKeyboard(FORWARD, global::gDeltaTime);
	if (keys[GLFW_KEY_S])
		global::gCamera->processKeyboard(BACKWARD, global::gDeltaTime);
	if (keys[GLFW_KEY_A])
		global::gCamera->processKeyboard(LEFT, global::gDeltaTime);
	if (keys[GLFW_KEY_D])
		global::gCamera->processKeyboard(RIGHT, global::gDeltaTime);
	if (keys[GLFW_KEY_N])
		global::gNext = true;
	if (keys[GLFW_KEY_M])
		global::gDisAssemble = true;
	if (keys[GLFW_KEY_K])
		global::gDisAssemble2 = true;
	*/
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
		pause = !pause;
	if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = global::gScreenWidth / 2.0;
		lastY = global::gScreenHeight / 2.0;
		firstMouse = false;
	}

	GLfloat xOffset = xpos - lastX;
	GLfloat yOffset = lastY - ypos;

	lastX = xpos;
	lastY = ypos;

	//global::gCamera->processMouseMovement(xOffset, yOffset);
	global::gCamera->setViewByMouse(xOffset, yOffset);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	//global::gCamera->processMouseScroll(yoffset);
}
#pragma endregion