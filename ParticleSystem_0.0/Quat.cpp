#include "Quat.h"

#include <cmath>

using namespace Quat;

Quaternion Quat::quatMult(Quaternion pOne, Quaternion pTwo)
{
	float lX = pOne.mW * pTwo.mX + pOne.mX * pTwo.mW + pOne.mY * pTwo.mZ - pOne.mZ * pTwo.mY;
	float lY = pOne.mW * pTwo.mY - pOne.mX * pTwo.mZ + pOne.mY * pTwo.mW + pOne.mZ * pTwo.mX;
	float lZ = pOne.mW * pTwo.mZ + pOne.mX * pTwo.mY - pOne.mY * pTwo.mX + pOne.mZ * pTwo.mW;
	float lW = pOne.mW * pTwo.mW - pOne.mX * pTwo.mX - pOne.mY * pTwo.mY - pOne.mZ * pTwo.mZ;
	return Quaternion(lX, lY, lZ, lW);
}

double Quat::quatLength(Quaternion pQuat)
{
	return sqrt(pQuat.mX * pQuat.mX + pQuat.mY * pQuat.mY + pQuat.mZ * pQuat.mZ + pQuat.mW * pQuat.mW);
}

Quaternion Quat::quatNormalize(Quaternion pQuat)
{
	double L = quatLength(pQuat);
	pQuat.mX /= L;
	pQuat.mY /= L;
	pQuat.mZ /= L;
	pQuat.mW /= L;
	return pQuat;
}

Quaternion Quat::quatConjugate(Quaternion pQuat)
{
	Quaternion toBeReturned;
	toBeReturned.mX = -pQuat.mX;
	toBeReturned.mY = -pQuat.mY;
	toBeReturned.mZ = -pQuat.mZ;
	toBeReturned.mW = pQuat.mW;
	return toBeReturned;
}