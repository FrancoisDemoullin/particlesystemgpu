#version 400 core

precision highp float;

in vec2 UV;

uniform sampler2D velocities;
uniform sampler2D read_from_me;

uniform float deltaTime;

out vec4 pos;

void main()
{
	vec4 oldPosition = texture(read_from_me, UV);
	vec3 updatedPosition = oldPosition.xyz + texture(velocities, UV).xyz * deltaTime;
	pos = vec4(updatedPosition, 1.0);
}