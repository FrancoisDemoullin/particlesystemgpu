#pragma once

#define GLEW_STATIC
#include <Gl/glew.h>
#include <GLFW/glfw3.h>

#include "Shader.h"

class PS 
{
public:
	PS(
		int pSize, 
		float pLife,
		GLfloat* pGoalPosArray,
		size_t pGoalPosSize,
		const GLchar* pPositionShaderVert, 
		const GLchar* pPositionShaderFrag, 
		const GLchar* pVelocityShaderVert, 
		const GLchar* pVelocityShaderFrag, 
		const GLchar* pRenderShaderVert,
		const GLchar* pRenderShaFrag
		);

	void renderPass();
	void updatePass();

	void hotSwapVelocityShader(char* newVertex, char* newFragment);
	void hotSwapPositionShader(char* newVertex, char* newFragment);

	inline void setGoalPos(GLfloat* pGoalPos) { mCurrentGoalPosArray = pGoalPos; }

private:

	void setUp();
	void setUpFirstPassBuffers();
	void setUpTextures();
	void setUpFBOs();
	void setUpRenderPass();
	void shaderSetup();

	GLuint mFirstPassVAO;
	GLuint mFirstPassVBO;
	GLuint mFirstPassEBO;

	GLuint mPositionA;
	GLuint mPositionB;

	GLuint mVelocityA;
	GLuint mVelocityB;

	GLuint mFboPositionA;
	GLuint mFboPositionB;
	GLuint mFboVelocityA;
	GLuint mFboVelocityB;

	GLuint mRenderPassVAO;
	GLuint mRenderPassVBO;
	GLuint mRenderPassEBO;

	GLuint mGoalPositionTexture;
	GLfloat* mCurrentGoalPosArray;
	size_t mGoalPosSize;

	Shader* mPositionUpdateShader;
	Shader* mVelocityUpdateShader;
	Shader* mRenderShader;

	Shader* mDebuggingShader;

	// render shaders are irrelevant, the just render
	const GLchar* mPositionShaderVert;
	const GLchar* mPositionShaderFrag;
	const GLchar* mVelocityShaderVert;
	const GLchar* mVelocityShaderFrag;
	const GLchar* mRenderShaderVert;
	const GLchar* mRenderShaderFrag;
	
	int mSize;
	float mLife;
	bool mPingPong;
};
