#pragma once

namespace Quat 
{
	struct Quaternion
	{
		float mX;
		float mY;
		float mZ;
		float mW;

		inline Quaternion(float pX = 0.0f, float pY = 0.0f, float pZ = 0.0f, float pW = 0.0f) : mX(pX), mY(pY), mZ(pZ), mW(pW) {}

		inline Quaternion operator=(Quaternion a)
		{
			mX = a.mX;
			mY = a.mY;
			mZ = a.mZ;
			mW = a.mW;
			return a;
		}

		inline Quaternion(const Quaternion & a)
		{
			mX = a.mX;
			mY = a.mY;
			mZ = a.mZ;
			mW = a.mW;
		}
	};

	Quaternion quatMult(Quaternion pOne, Quaternion pTwo);
	double quatLength(Quaternion pQuat);
	Quaternion quatNormalize(Quaternion pQuat);
	Quaternion quatConjugate(Quaternion pQuat);
}

