#version 400 core

precision highp float;

in vec2 UV;

uniform sampler2D degubMe;

out vec4 color;

void main()
{
	vec4 tex = texture(degubMe, UV);

	vec4 Color;
	if (tex.w < 0.5)
	{
		Color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
	}
	else if (tex.w < 0.9)
	{
		Color = vec4(0.0f, 1.0f, 0.0f, 1.0f);
	}
	else if (tex.w == 1.0)
	{
		Color = vec4(0.0f, 0.0f, 1.0f, 1.0f);
	}

	color = Color;
}