#version 400 core

precision highp float;

in vec2 UV;

uniform sampler2D read_from_me;
uniform sampler2D goal_position;
uniform sampler2D cur_position;
uniform float deltaTime;

out vec4 new_velocity;

void main()
{
	vec4 tex = texture(read_from_me, UV);

	vec3 goal_pos = texture(goal_position, UV).xyz;
	vec3 cur_pos = texture(cur_position, UV).xyz;

	vec3 old_velocity = tex.xyz;
	float old_life = tex.w;

	vec3 updated_velocity;
	//updated_velocity = old_velocity + vec3(0.0, 0.0, -0.1);
	
	updated_velocity = (60 * (goal_pos - cur_pos) * deltaTime); // old_velocity +

	new_velocity = vec4(updated_velocity, 1.0f); // vec4(updated_velocity, life);
}