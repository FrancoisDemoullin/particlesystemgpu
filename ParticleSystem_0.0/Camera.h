#ifndef CAMERA_H
#define CAMERA_H

#include <vector>

// GL Includes
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Enumeration of all possible Camera movements
enum Camera_Movement 
{
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT
};

// Default camera values
const GLfloat YAW = -90.0f;
const GLfloat PITCH = 0.0f;
const GLfloat SPEED = 3.0f;
const GLfloat SENSITIVTY = 0.25f;
const GLfloat ZOOM = 45.0f;

class Camera
{
	public:
		// Camera Attributes
		glm::vec3 Position;
		glm::vec3 Front;
		glm::vec3 Up;
		glm::vec3 Right;
		glm::vec3 WorldUp;
		// Eular Angles
		GLfloat Yaw;
		GLfloat Pitch;
		// Camera options
		GLfloat MovementSpeed;
		GLfloat MouseSensitivity;
		GLfloat Zoom;

		// Constructors
		Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = YAW, GLfloat pitch = PITCH);
		//Camera(glm::vec3 position, glm::vec3 up , GLfloat yaw, GLfloat pitch);
		Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch);

		// Process user input
		void processKeyboard(Camera_Movement direction, GLfloat deltaTime);
		void processMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true);
		void processMouseScroll(GLfloat yoffset);

		// Returns the view matrix calculated using Eular Angles and the LookAt Matrix
		inline glm::mat4 GetViewMatrix()
		{
			return glm::lookAt(this->Position, this->Position + this->Front, this->Up);
		}

		//right vector getter
		inline glm::vec3 GetRightVector()
		{
			return Right;
		}

	private:
		// Calculates the front vector
		void updateCameraVectors();
};

#endif