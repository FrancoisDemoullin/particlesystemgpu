#include <stdlib.h>
#include <iostream>

#include "PS.h"
#include "global.h"

PS::PS(int pSize, float pLife, GLfloat* pGoalPosArray, size_t pGoalPosSize, const GLchar* pPositionShaderVert, const GLchar* pPositionShaderFrag, const GLchar* pVelocityShaderVert, const GLchar* pVelocityShaderFrag, const GLchar* pRenderShaderVert, const GLchar* pRenderShaderFrag)
	: mSize(pSize), mLife(pLife), mPingPong(true), mPositionShaderVert(pPositionShaderVert), mPositionShaderFrag(pPositionShaderFrag), mVelocityShaderVert(pVelocityShaderVert), mVelocityShaderFrag(pVelocityShaderFrag), mGoalPosSize(pGoalPosSize), mRenderShaderVert(pRenderShaderVert), mRenderShaderFrag(pRenderShaderFrag)
{
	mCurrentGoalPosArray = pGoalPosArray;
	setUp();
}

void PS::setUp()
{
	setUpFirstPassBuffers();
	setUpTextures();
	setUpFBOs();
	setUpRenderPass();
	shaderSetup();
}

void PS::setUpFirstPassBuffers()
{
	// set up first pass buffer
	glGenVertexArrays(1, &mFirstPassVAO);
	glBindVertexArray(mFirstPassVAO);

	glGenBuffers(1, &mFirstPassVBO);
	glBindBuffer(GL_ARRAY_BUFFER, mFirstPassVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(global::first_pass_vertices), global::first_pass_vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &mFirstPassEBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mFirstPassEBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(global::indices), global::indices, GL_STATIC_DRAW);

	GLuint mGoalPositionVBO;
	glGenBuffers(1, &mGoalPositionVBO);
	glBindBuffer(GL_ARRAY_BUFFER, mGoalPositionVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(global::first_pass_vertices), global::first_pass_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

void PS::setUpTextures()
{
	glGenTextures(1, &mPositionA);
	glBindTexture(GL_TEXTURE_2D, mPositionA);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	// starting positon of particles is set here
	// set initial position to vec3(0, 0, 0)
	//GLfloat* initialPosition = (GLfloat*)malloc(mSize * mSize * 3 * sizeof(GLfloat));
	GLfloat* initialPosition = mCurrentGoalPosArray;

	/*
	for (int i = 0; i < mSize * mSize * 3; i+=3) 
	{
		if (i % 2 == 0) 
		{
			initialPosition[i] = 900 * static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 600));
			initialPosition[i + 1] = 900 * static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 600));
			initialPosition[i + 2] = 900 * static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 600));
		}
		else
		{
			initialPosition[i] = -900 * static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 600));
			initialPosition[i + 1] = -900 * static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 600));
			initialPosition[i + 2] = 1800 * static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 600));
		}
	}
	*/
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, mSize, mSize, 0, GL_RGB, GL_FLOAT, initialPosition);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &mPositionB);
	glBindTexture(GL_TEXTURE_2D, mPositionB);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	// no initial data, we'll write to this texture at the very first iteration of the game loop
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, mSize, mSize, 0, GL_RGB, GL_FLOAT, initialPosition);
	glBindTexture(GL_TEXTURE_2D, 0);

	// velocity, contains life too

	glGenTextures(1, &mVelocityA);
	glBindTexture(GL_TEXTURE_2D, mVelocityA);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	GLfloat* initialVelocity = (GLfloat*)malloc(mSize * mSize * 4 * sizeof(GLfloat));
	for (int i = 0; i < mSize * mSize * 4; i = i + 4)
	{
		// filling the texture array
		initialVelocity[i] = 0;
		initialVelocity[i + 1] = 0;
		initialVelocity[i + 2] = 0;
		// life
		float debugging = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 100));
		initialVelocity[i + 3] = 0.5f;//debugging;
	}
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, mSize, mSize, 0, GL_RGBA, GL_FLOAT, initialVelocity);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &mVelocityB);
	glBindTexture(GL_TEXTURE_2D, mVelocityB);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, mSize, mSize, 0, GL_RGBA, GL_FLOAT, initialVelocity);
	glBindTexture(GL_TEXTURE_2D, 0);


	// Goal position vec3 texture
	glGenTextures(1, &mGoalPositionTexture);
	glBindTexture(GL_TEXTURE_2D, mGoalPositionTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, mSize, mSize, 0, GL_RGB, GL_FLOAT, mCurrentGoalPosArray);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void PS::setUpFBOs()
{
	// FBO's
	glGenFramebuffers(1, &mFboPositionA);
	glBindFramebuffer(GL_FRAMEBUFFER, mFboPositionA);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mPositionA, 0);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "FRAMEBUFFER setup went wrong" << std::endl;
	}

	glGenFramebuffers(1, &mFboPositionB);
	glBindFramebuffer(GL_FRAMEBUFFER, mFboPositionB);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mPositionB, 0);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "FRAMEBUFFER setup went wrong" << std::endl;
	}

	glGenFramebuffers(1, &mFboVelocityA);
	glBindFramebuffer(GL_FRAMEBUFFER, mFboVelocityA);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mVelocityA, 0);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "FRAMEBUFFER setup went wrong" << std::endl;
	}

	glGenFramebuffers(1, &mFboVelocityB);
	glBindFramebuffer(GL_FRAMEBUFFER, mFboVelocityB);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mVelocityB, 0);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "FRAMEBUFFER setup went wrong" << std::endl;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void PS::setUpRenderPass()
{
	//complete screen mesh
	size_t sizeOfRenderVertices = 16 * mSize * mSize * sizeof(GLfloat);
	GLfloat* renderVertices = new GLfloat[sizeOfRenderVertices];

	// fill the buffer
	int horizontalHelper = 0.0f;
	int verticalHelper = 0.0f;
	for (int i = 0; i <= mSize * mSize * 16; i = i + 16)
	{
		if (horizontalHelper == mSize)
		{
			verticalHelper = verticalHelper + 1.0f;
			horizontalHelper = 0.0f;
		}

		renderVertices[i] = 1.0f;  // top right
		renderVertices[i + 1] = 1.0f;
		renderVertices[i + 2] = (GLfloat)verticalHelper / (GLfloat)(mSize - 1);
		renderVertices[i + 3] = (GLfloat)horizontalHelper / (GLfloat)(mSize - 1);
		renderVertices[i + 4] = 1.0f;  // bottom right
		renderVertices[i + 5] = -1.0f;
		renderVertices[i + 6] = (GLfloat)verticalHelper / (GLfloat)(mSize - 1);
		renderVertices[i + 7] = (GLfloat)horizontalHelper / (GLfloat)(mSize - 1);
		renderVertices[i + 8] = -1.0f;  // bottom left
		renderVertices[i + 9] = -1.0f;
		renderVertices[i + 10] = (GLfloat)verticalHelper / (GLfloat)(mSize - 1);
		renderVertices[i + 11] = (GLfloat)horizontalHelper / (GLfloat)(mSize - 1);
		renderVertices[i + 12] = -1.0f;  // top left
		renderVertices[i + 13] = 1.0f;
		renderVertices[i + 14] = (GLfloat)verticalHelper / (GLfloat)(mSize - 1);
		renderVertices[i + 15] = (GLfloat)horizontalHelper / (GLfloat)(mSize - 1);

		horizontalHelper = horizontalHelper + 1.0f;
	}

	size_t sizeOfRenderIndices = 6 * mSize * mSize * sizeof(GLuint);
	GLuint* renderIndices = new GLuint[sizeOfRenderIndices];
	int increment = 0;
	for (GLuint i = 0; i <= mSize * mSize * 6; i = i + 6)
	{
		renderIndices[i] = 0 + increment;  //first Triangle
		renderIndices[i + 1] = 1 + increment;
		renderIndices[i + 2] = 3 + increment;
		renderIndices[i + 3] = 1 + increment;  // second Triangle
		renderIndices[i + 4] = 2 + increment;
		renderIndices[i + 5] = 3 + increment;

		increment += 4;
	};

	glGenVertexArrays(1, &mRenderPassVAO);
	glBindVertexArray(mRenderPassVAO);

	glGenBuffers(1, &mRenderPassVBO);
	glBindBuffer(GL_ARRAY_BUFFER, mRenderPassVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeOfRenderVertices, renderVertices, GL_STATIC_DRAW);

	// not needed anymore
	delete[] renderVertices;

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &mRenderPassEBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mRenderPassEBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeOfRenderIndices, renderIndices, GL_STATIC_DRAW);

	delete[] renderIndices;

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void PS::shaderSetup()
{
	mPositionUpdateShader = new Shader(mPositionShaderVert, mPositionShaderFrag);
	mVelocityUpdateShader = new Shader(mVelocityShaderVert, mVelocityShaderFrag);
	mRenderShader = new Shader(mRenderShaderVert, mRenderShaderFrag);

	mDebuggingShader = new Shader("Passthrough.vert", "DebuggingFragmentShader.glsl");
}

void PS::renderPass()
{
	// render pass
	mRenderShader->use();

	glViewport(0, 0, global::gScreenWidth, global::gScreenHeight);

	glBindVertexArray(mRenderPassVAO);

	// Camera and View related matrices
	//glm::mat4 projection = glm::perspective(global::gCamera->Zoom, (float)global::gScreenWidth / (float)global::gScreenHeight, 0.1f, 100.0f);

	glm::mat4 projection = glm::perspective(45.0f, (float)global::gScreenWidth / (float)global::gScreenHeight, 0.1f, 100.0f);

	glm::mat4 view = global::gCamera->getViewMatrix();
	//glm::mat4 view = global::gCamera->GetViewMatrix();

	GLuint Projection_ID = glGetUniformLocation(mRenderShader->Program, "projection");
	GLuint View_ID = glGetUniformLocation(mRenderShader->Program, "view");
	glUniformMatrix4fv(Projection_ID, 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(View_ID, 1, GL_FALSE, glm::value_ptr(view));

	GLuint CameraRight_worldspace_ID = glGetUniformLocation(mRenderShader->Program, "CameraRight_worldspace");
	GLuint CameraUp_worldspace_ID = glGetUniformLocation(mRenderShader->Program, "CameraUp_worldspace");

	glm::vec3 lRight = glm::normalize(glm::cross(glm::normalize(global::gCamera->getView() - global::gCamera->getPos()), glm::normalize(global::gCamera->getUp())));
	//glm::vec3 lRight = glm::normalize(glm::cross(global::gCamera->Front, global::gCamera->Up));
	//glm::vec3 lRight = glm::normalize(glm::cross(glm::normalize(global::gCamera->Front), glm::vec3(0.0, 1.0, 0.0)));
	//glm::vec3 lRight = global::gCamera->Right;

	glm::vec3 lUp = glm::normalize(global::gCamera->getUp());
	//glm::vec3 lUp = global::gCamera->Up;

	glUniform3f(CameraRight_worldspace_ID, lRight.x, lRight.y, lRight.z);
	glUniform3f(CameraUp_worldspace_ID, lUp.x, lUp.y, lUp.z);

	glActiveTexture(GL_TEXTURE0);

	// pass the texture we just wrote to as a uniform for rendering
	if (mPingPong)
	{
		glBindTexture(GL_TEXTURE_2D, mPositionA);
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D, mPositionB);
	}

	glUniform1i(glGetUniformLocation(mRenderShader->Program, "position_texture"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, mGoalPositionTexture);
	glUniform1i(glGetUniformLocation(mRenderShader->Program, "goal_texture"), 1);

	glDrawElements(GL_TRIANGLES, 1000000 * 6, GL_UNSIGNED_INT, nullptr);

	glBindVertexArray(0);

	// flip textures around for next iteration
	//mPingPong = mPingPong;
}

void PS::updatePass()
{
	// abstracted values for the ping pong iteration
	GLuint lVelocityFbo;
	GLuint lVelocityTexture;

	GLuint lPositionFbo;
	GLuint lVelocityTextureForPositionPass;
	GLuint lPositionUniformTexture;
	GLuint lPositionWriteToTexture;

	if (mPingPong)
	{
		lVelocityFbo = mFboVelocityB;
		lVelocityTexture = mVelocityA;
		lVelocityTextureForPositionPass = mVelocityB;
		lPositionFbo = mFboPositionB;
		lPositionUniformTexture = mPositionA;
		lPositionWriteToTexture = mPositionB;
	}
	else
	{
		lVelocityFbo = mFboVelocityA;
		lVelocityTexture = mVelocityB;
		lVelocityTextureForPositionPass = mVelocityA;
		lPositionFbo = mFboPositionA;
		lPositionUniformTexture = mPositionB;
		lPositionWriteToTexture = mPositionA;
	}

	//
	// velocityPass
	// 

	mVelocityUpdateShader->use();

	glBindFramebuffer(GL_FRAMEBUFFER, lVelocityFbo);

	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);

	// change viewport to match the texture
	glViewport(0, 0, mSize, mSize);

	// pass velocity as a uniform
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, lVelocityTexture);
	glUniform1i(glGetUniformLocation(mVelocityUpdateShader->Program, "read_from_me"), 0);

	// pass goal position as a uniform
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, mGoalPositionTexture);
	glUniform1i(glGetUniformLocation(mVelocityUpdateShader->Program, "goal_position"), 1);

	// pass current position as a uniform
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, lPositionUniformTexture);
	glUniform1i(glGetUniformLocation(mVelocityUpdateShader->Program, "cur_position"), 2);

	// pass deltaTime as a uniform
	glUniform1f(glGetUniformLocation(mVelocityUpdateShader->Program, "deltaTime"), global::gDeltaTime);

	glUniform1f(glGetUniformLocation(mVelocityUpdateShader->Program, "increment"), global::gIncrement);
	global::gIncrement++;


	glBindVertexArray(mFirstPassVAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	//
	// Debugging the texture
	//

	/*
	// change viewport to match the screen
	glViewport(0, 0, global::gScreenWidth, global::gScreenHeight);

	mDebuggingShader->use();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, lVelocityTextureForPositionPass);

	glUniform1i(glGetUniformLocation(mDebuggingShader->Program, "debugMe"), 0);

	glBindVertexArray(mFirstPassVAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);

	return;
	*/

	//
	// position Pass
	//

	mPositionUpdateShader->use();

	glBindFramebuffer(GL_FRAMEBUFFER, lPositionFbo);

	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);

	// change viewport to match the texture
	glViewport(0, 0, mSize, mSize);

	// we just wrote to lVelocityTextureForPositionPass, thus pass it as a uniform to update the position
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, lVelocityTextureForPositionPass);
	glUniform1i(glGetUniformLocation(mPositionUpdateShader->Program, "velocities"), 0);

	// pass positionA as a uniform
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, lPositionUniformTexture);
	glUniform1i(glGetUniformLocation(mPositionUpdateShader->Program, "read_from_me"), 1);

	// pass deltaTime as a uniform
	glUniform1f(glGetUniformLocation(mPositionUpdateShader->Program, "deltaTime"), global::gDeltaTime);

	glBindVertexArray(mFirstPassVAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	mPingPong = !mPingPong;
}

void PS::hotSwapVelocityShader(char * newVertex, char * newFragment)
{
	// hot swap the velocity shader
	mVelocityUpdateShader->~Shader();

	mVelocityUpdateShader = new Shader(newVertex, newFragment);
}

void PS::hotSwapPositionShader(char * newVertex, char * newFragment)
{
	// hot swap the velocity shader
	mPositionUpdateShader->~Shader();

	mPositionUpdateShader = new Shader(newVertex, newFragment);
}
