#pragma once

#include "Camera.h"
#include "QuaternionCamera.h"

#define GLEW_STATIC
#include <Gl/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtc/random.hpp>

namespace global 
{
	//complete screen mesh
	static GLfloat first_pass_vertices[] = {
		// Positions          // Tex 
		1.0f,  1.0f, 0.0f, 1.0f, 1.0f, // Top Right
		1.0f, -1.0f, 0.0f, 1.0f, 0.0f, // Bottom Right
		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f, // Bottom Left
		-1.0f,  1.0f, 0.0f, 0.0f, 1.0f  // Top Left 
	};

	static GLuint indices[] = {
		0, 1, 3, // First Triangle
		1, 2, 3  // Second Triangle
	};

	extern GLuint gScreenHeight;
	extern GLuint gScreenWidth;
	// extern Camera* gCamera;
	extern QuatCamera* gCamera;

	extern float gDeltaTime;
	extern bool gNext;
	extern bool gDisAssemble;

	extern bool gDisAssemble2;
	extern float gIncrement;

	extern glm::mat4 gProjection;
	extern glm::mat4 gView;
}
