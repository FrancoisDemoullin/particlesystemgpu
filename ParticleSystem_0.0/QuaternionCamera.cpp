#include "QuaternionCamera.h"

using namespace Quat;

QuatCamera::QuatCamera(glm::vec3 pPosition) : mViewVec(-glm::normalize(pPosition)), mPosition(pPosition), mUp(0.0f, 1.0f, 0.0f), lCurrentRotationX(0.0f)
{
}

void QuatCamera::rotateCamera(double Angle, double x, double y, double z)
{
	Quaternion temp, quat_view, result;

	temp.mX = x * sin(Angle / 2.0);
	temp.mY = y * sin(Angle / 2.0);
	temp.mZ = z * sin(Angle / 2.0);
	temp.mW = cos(Angle / 2.0);

	quat_view.mX = mViewVec.x;
	quat_view.mY = mViewVec.y;
	quat_view.mZ = mViewVec.z;
	quat_view.mW = 0.0;


	Quaternion intermediate = quatMult(temp, quat_view);
	Quaternion conjugate = quatConjugate(temp);
	result = quatMult(intermediate, conjugate);

	glm::vec3 tempView(result.mX, result.mY, result.mZ);

 	mViewVec = tempView;
}

void QuatCamera::setViewByMouse(float xOffset, float yOffset)
{
	if ((xOffset == 0) && (yOffset == 0))
	{
		return;
	}
	glm::vec2 mouseDirection(0, 0);
	mouseDirection.x = xOffset / 1000;
	mouseDirection.y = yOffset / 1000;

	
	glm::vec3 lAxis = glm::cross(mViewVec, mUp); //- mPosition
	lAxis = glm::normalize(lAxis);
	rotateCamera(mouseDirection.y, lAxis.x, lAxis.y, lAxis.z);
	rotateCamera(mouseDirection.x, 0, 1, 0);
}

glm::mat4 QuatCamera::getViewMatrix()
{
	glm::mat4 debugging = glm::lookAt(mPosition, mPosition + mViewVec, mUp);
	//glm::mat4 debugging = glm::lookAt(glm::vec3(), glm::vec3(), glm::vec3());
	return debugging;
}
